

#ifndef __CRC32_H
#define __CRC32_H

uint32_t crc32(uint32_t crc, uint8_t *buf, int len);

#endif // __CRC32_H